# MAX.gov Design System

This is the repository for MAX.gov’s Design System for developing web
products. It contains both the assets and the content for the site.

MAX.gov strives to better serve our users by using plain language; relying
on simple, user-friendly design; and providing easy access to services and
tools that make them more effective in their daily work.

This Design System is an open-source resource for MAX.gov staff to produce
effective and visually-consistent applications that are easy for government staff
and their associates to access, use, and understand. The System includes our
design principles, guidelines for user experience, visual identity standards, and
code snippets for common user interface elements. The System will continue to
evolve as we learn what works best for the MAX.gov and the users we serve.

In addition to the informational sections of the Design System, our goal is to
create a resource for MAX.gov developers to use when creating and modifying
applications. This UI component library will contain all of the most common
interface elements used by our applications, and over time, will become a
one-stop shop for elements of any MAX.gov interface.

These standards reflect our latest thinking and are just beginning to be
incorporated into MAX.gov’s most recent projects.

We are proud to join the community of organizations that have made their
design standards public, such as
[Mozilla](http://www.mozilla.org/en-US/styleguide/),
[BBC](http://www.bbc.co.uk/gel), and the UK’s
[Government Digital Service](https://www.gov.uk/service-manual).
We hope our design system can serve as a foundation for discussing and
practicing user-centered design in government.

All content has been released as open source under the CC0 1.0 Universal
Public Domain Dedication, and we’d love for other agencies, developers, or
groups to adapt it for their own use.


## Running it Locally

Content editors and developers probably want to set up the Design System
on their local machine so they can preview updates without pushing to the Git
repository.

Before you get started make sure you have an up-to-date version of
[NPM](https://www.npmjs.com/), which is bundled with
[Node.js](https://nodejs.org/download/).

[Fork and clone the repo](https://help.github.com/articles/fork-a-repo/) to
your local machine.

From the project directory, run NPM install:

```sh
npm install
```

This will install the proper versions of all the necessary NPM packages.

Point your web server configuration (we use [MAMP](https://www.mamp.info/)
for local development) at the **/public** directory to view the site.


## Working with the Front End

The Design System front end currently uses the following:

- [NPM](https://www.npmjs.com/): Package manager for front-end
   dependencies.
- [Gulp](http://gulpjs.com/): Task runner for pulling in assets, linting and
   concatenating code, etc.
- [Sass](http://sass-lang.com/): CSS pre-processor.
- [Bootstrap](http://getbootstrap.com/): HTML, CSS, and JS framework for
  developing responsive, mobile first projects on the web.


### Installing Dependencies (One Time)

1. Install [Node.js](http://nodejs.org/) however you’d like.
2. Install [Gulp](http://gulpjs.com/) globally:

```sh
npm install gulp --global
```

### Developing

When first setting up this project, and each time you fetch from upstream,
install project dependencies with NPM and run `gulp` to build everything:

```sh
npm install
```

We use [Gulp](http://gulpjs.com/) to compile and compress our Sass and
JavaScript files. The default Gulp task will watch for changes and update
the site whenever you save a file:

```
gulp
```


## Roadmap

In the near future we plan to develop complete UI component libraries for all of
our development platforms.
