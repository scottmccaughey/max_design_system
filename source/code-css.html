<!DOCTYPE html>

<!--[if IE 8 ]> <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-us" class="no-js"><!--<![endif]-->

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CSS | Code Guide | MAX Design System</title>

    @@include('htmlhead.inc')

  </head>

  <body id="top">

    @@include('max-toolbar.inc')
    @@include('header.inc')

    <main class="container">

      <div class="row">

        <div class="col-sm-4 col-md-3">

          @@include('nav.inc')

        </div>

        <div class="col-sm-8 col-md-9">
          <div id="main" class="main-content main-content-css" tabindex="-1">
            <h1>Code Guide</h1>
            <h2>CSS</h2>
            <ul class="nav nav-pills nav-pills-menu">
              <li role="presentation"><a href="#css-syntax">Syntax</a></li>
              <li role="presentation"><a href="#css-declarations">Declarations</a></li>
              <li role="presentation"><a href="#css-classes">Class Names</a></li>
              <li role="presentation"><a href="#css-selectors">Selectors</a></li>
              <li role="presentation"><a href="#css-organization">Organization</a></li>
              <li role="presentation"><a href="#css-comments">Comments</a></li>
              <li role="presentation"><a href="#css-media-queries">Media Queries</a></li>
              <li role="presentation"><a href="#css-sass">Sass</a></li>
            </ul>
            <article class="pad-top">
              <section class="row">
                <div class="col-md-8">
                  <p>Standards for developing flexible, durable, and sustainable CSS.</p>
                  <blockquote>Every line of code should appear to be written by a single person, no matter the number of contributors.</blockquote>
                </div>
                <div class="col-md-4">
                  <div class="well">
                    <p><em>Adapted from <a href="http://codeguide.co/" target="_blank">Code Guide</a> by @mdo.</em></p>
                  </div>
                </div>
              </section>
            </article>
            <article id="css-syntax">
              <h3>Syntax</h3>
              <section class="row">
                <div class="col-md-6">
                  <ul>
                    <li>Use soft tabs with two spaces—they're the only way to guarantee code renders the same in any environment.</li>
                    <li>When grouping selectors, keep individual selectors to a single line.</li>
                    <li>Include one space before the opening brace of declaration blocks for legibility.</li>
                    <li>Place closing braces of declaration blocks on a new line.</li>
                    <li>Include one space after <code>:</code> for each declaration.</li>
                    <li>Each declaration should appear on its own line for more accurate error reporting.</li>
                    <li>End all declarations with a semi-colon. The last declaration's is optional, but your code is more error prone without it.</li>
                    <li>Comma-separated property values should include a space after each comma (e.g., <code>box-shadow</code>).</li>
                    <li>Don't include spaces after commas <em>within</em> <code>rgb()</code>, <code>rgba()</code>, <code>hsl()</code>, <code>hsla()</code>, or <code>rect()</code> values. This helps differentiate multiple color values (comma, no space) from multiple property values (comma with space).</li>
                    <li>Don't prefix property values or color parameters with a leading zero (e.g., <code>.5</code> instead of <code>0.5</code> and <code>-.5px</code> instead of <code>-0.5px</code>).</li>
                    <li>Lowercase all hex values, e.g., <code>#fff</code>. Lowercase letters are much easier to discern when scanning a document as they tend to have more unique shapes.</li>
                    <li>Use shorthand hex values where available, e.g., <code>#fff</code> instead of <code>#ffffff</code>.</li>
                    <li>Quote attribute values in selectors, e.g., <code>input[type="text"]</code>. <a href="http://mathiasbynens.be/notes/unquoted-attribute-values#css">They’re only optional in some cases</a>, and it’s a good practice for consistency.</li>
                    <li>Avoid specifying units for zero values, e.g., <code>margin: 0;</code> instead of <code>margin: 0px;</code>.</li>
                  </ul>
                  <p>Questions on the terms used here? See the <a href="http://en.wikipedia.org/wiki/Cascading_Style_Sheets#Syntax" target="_blank">syntax section of the Cascading Style Sheets article</a> on Wikipedia.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-css">
/* Bad CSS */
.selector, .selector-secondary, .selector[type=text] {
  padding:15px;
  margin:0px 0px 15px;
  background-color:rgba(0, 0, 0, 0.5);
  box-shadow:0px 1px 2px #CCC,inset 0 1px 0 #FFFFFF
}

/* Good CSS */
.selector,
.selector-secondary,
.selector[type="text"] {
  padding: 15px;
  margin-bottom: 15px;
  background-color: rgba(0,0,0,.5);
  box-shadow: 0 1px 2px #ccc, inset 0 1px 0 #fff;
}</code></pre>
                </div>
              </section>
              <section class="row">
                <h4>Don’t Use <code>@import</code></h4>
                <div class="col-md-6">
                  <p>Compared to <code>&lt;link&gt;</code>, <code>@import</code> is slower, adds extra page requests, and can cause other unforeseen problems. Avoid them and instead opt for an alternate approach:</p>
                  <ul>
                    <li>Use multiple <code>&lt;link&gt;</code> elements</li>
                    <li>Compile your CSS with a preprocessor like Sass into a single file</li>
                    <li>Concatenate your CSS files with features provided in Node, Rails, and other environments</li>
                  </ul>
                  <p>For more information, <a href="http://www.stevesouders.com/blog/2009/04/09/dont-use-import/" target="_blank">read this article by Steve Souders</a>.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-markup">
&lt;!-- Use link elements --&gt;
&lt;link rel=&quot;stylesheet&quot; href=&quot;core.css&quot;&gt;

&lt;!-- Avoid @imports --&gt;
&lt;style&gt;
  @import url('more.css');
&lt;/style&gt;</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-declarations">
              <h3>Declarations</h3>
              <section class="row">
                <h4>Declaration Order</h4>
                <div class="col-md-6">
                  <p>Related property declarations should be grouped together following the order:</p>
                  <ol>
                    <li>Positioning</li>
                    <li>Box model</li>
                    <li>Typographic</li>
                    <li>Visual</li>
                  </ol>
                  <p>Positioning comes first because it can remove an element from the normal flow of the document and override box model related styles. The box model comes next as it dictates a component's dimensions and placement.</p>
                  <p>Everything else takes place <em>inside</em> the component or without impacting the previous two sections, and thus they come last.</p>
                  <p>For a complete list of properties and their order, please see <a href="http://twitter.github.com/recess" target="_blank">Recess</a>.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-css">
.declaration-order {
  /* Positioning */
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;

  /* Box-model */
  display: block;
  float: right;
  width: 100px;
  height: 100px;

  /* Typography */
  font: normal 13px 'Open Sans';
  line-height: 1.5;
  color: #333;
  text-align: center;

  /* Visual */
  background-color: #f5f5f5;
  border: 1px solid #e5e5e5;
  border-radius: 3px;

  /* Misc */
  opacity: 1;
}</code></pre>
                </div>
              </section>
              <section class="row">
                <h4>Rules with Single Declarations</h4>
                <div class="col-md-6">
                  <p>In instances where a rule set includes <strong>only one declaration</strong>, consider removing line breaks for readability and faster editing. Any rule set with multiple declarations should be split to separate lines.</p>
                  <p>The key factor here is error detection—e.g., a CSS validator stating you have a syntax error on Line 183. With a single declaration, there's no missing it. With multiple declarations, separate lines is a must for your sanity.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-css">
/* Single declarations on one line */
.icon       { background-position: 0 0; }
.icon-home  { background-position: 0 -20px; }
.icon-about { background-position: 0 -40px; }

/* Multiple declarations, one per line */
.sprite {
  display: inline-block;
  width: 16px;
  height: 15px;
  background-image: url('../img/sprite.png');
}</code></pre>
                </div>
              </section>
              <section class="row">
                <h4>Shorthand Notation</h4>
                <div class="col-md-6">
                  <p>Strive to limit use of shorthand declarations to instances where you must explicitly set all the available values. Common overused shorthand properties include:</p>
                  <ul>
                    <li><code>padding</code></li>
                    <li><code>margin</code></li>
                    <li><code>font</code></li>
                    <li><code>background</code></li>
                    <li><code>border</code></li>
                    <li><code>border-radius</code></li>
                  </ul>
                  <p>Often times we don't need to set all the values a shorthand property represents. For example, HTML headings only set top and bottom margin, so when necessary, only override those two values. Excessive use of shorthand properties often leads to sloppier code with unnecessary overrides and unintended side effects.</p>
                  <p>The Mozilla Developer Network has a great article on <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties" target="_blank">shorthand properties</a> for those unfamiliar with notation and behavior.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-css">
/* Bad example */
.element {
  margin: 0 0 10px;
  background: red;
  background: url('image.jpg');
  border-radius: 3px 3px 0 0;
}

/* Good example */
.element {
  margin-bottom: 10px;
  background-color: red;
  background-image: url('image.jpg');
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}</code></pre>
                </div>
              </section>
              <section class="row">
                <h4>Prefixed Properties</h4>
                <div class="col-md-6">
                  <p>When using vendor prefixed properties, indent each property such that the declaration's value lines up vertically for easy multi-line editing.</p>
                  <p>In Sublime Text 2, use <strong>Selection &rarr; Add Previous Line</strong> (&#8963;&#8679;&uarr;) and <strong>Selection &rarr;  Add Next Line</strong> (&#8963;&#8679;&darr;). In Textmate, use <strong>Text &rarr; Edit Each Line in Selection</strong> (&#8963;&#8984;A).</p>
                  <div class="well well-style well-info">
                    <h5>Autoprefixer</h5>
                    <p>Preferably, use a postprocessor like <a href="https://github.com/postcss/autoprefixer" target="_blank">Autoprefixer</a> to automatically add any applicable vendor prefixes so that your code stays clean.</p>
                  </div>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-css">
/* Prefixed properties */
.selector {
  -webkit-box-flex: 1;
     -moz-box-flex: 1;
              flex: 1;
}</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-classes">
              <h3>Class Names</h3>
              <section class="row">
                <div class="col-md-6">
                  <ul>
                    <li>Keep classes lowercase and use dashes (not underscores or camelCase). Dashes serve as natural breaks in related class (e.g., <code>.btn</code> and <code>.btn-danger</code>).</li>
                    <li>Avoid excessive and arbitrary shorthand notation. <code>.btn</code> is useful for <em>button</em>, but <code>.s</code> doesn't mean anything.</li>
                    <li>Keep classes as short and succinct as possible.</li>
                    <li>Use meaningful names; use structural or purposeful names over presentational.</li>
                    <li>Prefix classes based on the closest parent or base class.</li>
                    <li>Use <code>.js-*</code> classes to denote behavior (as opposed to style), but keep these classes out of your CSS.</li>
                  </ul>
                  <p>It's also useful to apply many of these same rules when creating Sass and Less variable names.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-scss">
/* Bad example */
.t { ... }
.red { ... }
.header { ... }

/* Good example */
.tweet { ... }
.important { ... }
.tweet-header { ... }</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-selectors">
              <h3>Selectors</h3>
              <section class="row">
                <div class="col-md-6">
                  <ul>
                    <li>Use classes over generic element tag for optimum rendering performance.</li>
                    <li>Avoid using several attribute selectors (e.g., <code>[class^="..."]</code>) on commonly occuring components. Browser performance is known to be impacted by these.</li>
                    <li>Keep selectors short and strive to limit the number of elements in each selector to three.</li>
                    <li>Scope classes to the closest parent <strong>only</strong> when necessary (e.g., when not using prefixed classes).</li>
                  </ul>
                  <p>Additional reading:</p>
                  <ul>
                    <li><a href="http://markdotto.com/2012/02/16/scope-css-classes-with-prefixes/" target="_blank">Scope CSS classes with prefixes</a></li>
                    <li><a href="http://markdotto.com/2012/03/02/stop-the-cascade/" target="_blank">Stop the cascade</a></li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-scss">
/* Bad example */
span { ... }
.page-container #stream .stream-item .tweet .tweet-header .username { ... }
.avatar { ... }

/* Good example */
.avatar { ... }
.tweet-header .username { ... }
.tweet .avatar { ... }</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-organization">
              <h3>Organization</h3>
              <section class="row">
                <div class="col-md-6">
                  <ul>
                    <li>Organize sections of code by component.</li>
                    <li>Develop a consistent commenting hierarchy.</li>
                    <li>Use consistent white space to your advantage when separating sections of code for scanning larger documents.</li>
                    <li>When using multiple CSS files, break them down by component instead of page. Pages can be rearranged and components moved.</li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-scss">
/*
 * Component section heading
 */

.element { ... }


/*
 * Component section heading
 *
 * Sometimes you need to include optional context for the entire component. Do that up here if it's important enough.
 */

.element { ... }

/* Contextual sub-component or modifer */
.element-heading { ... }</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-comments">
              <h3>Comments</h3>
              <section class="row">
                <div class="col-md-6">
                  <p>Code is written and maintained by people. Ensure your code is descriptive, well commented, and approachable by others. Great code comments convey context or purpose. Do not simply reiterate a component or class name.</p>
                  <p>Be sure to write in complete sentences for larger comments and succinct phrases for general notes.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-scss">
/* Bad example */
/* Modal header */
.modal-header {
  ...
}

/* Good example */
/* Wrapping element for .modal-title and .modal-close */
.modal-header {
  ...
}</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-media-queries">
              <h3>Media Queries</h3>
              <section class="row">
                <div class="col-md-6">
                  <p>Place media queries as close to their relevant rule sets whenever possible. Don't bundle them all in a separate stylesheet or at the end of the document. Doing so only makes it easier for folks to miss them in the future.</p>
                  <div class="well well-style well-info">
                    <h5>Preprocessors</h5>
                    <p>Preferably, use a preprocessor like Sass where media queries can be nested inside the appropriate rule sets.</p>
                  </div>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-css">
.element { ... }
.element-avatar { ... }
.element-selected { ... }

@media (min-width: 480px) {
  .element { ... }
  .element-avatar { ... }
  .element-selected { ... }
}</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
            <article id="css-sass">
              <h3>Sass</h3>
              <section class="row">
                <h4>Nesting in Sass</h4>
                <div class="col-md-6">
                  <p>Avoid unnecessary nesting. Just because you can nest, doesn't mean you always should. Consider nesting only if you must scope styles to a parent and if there are multiple elements to be nested.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-scss">
/* Without nesting */
.table > thead > tr > th { … }
.table > thead > tr > td { … }

/* With nesting */
.table > thead > tr {
  > th { … }
  > td { … }
}</code></pre>
                </div>
              </section>
              <section class="row">
                <h4>Operators in Sass</h4>
                <div class="col-md-6">
                  <p>For improved readability, wrap all math operations in parentheses with a single space between values, variables, and operators.</p>
                </div>
                <div class="col-md-6">
                  <pre><code class="language-scss">
/* Bad example */
.element {
  margin: 10px 0 @variable*2 10px;
}

/* Good example */
.element {
  margin: 10px 0 (@variable * 2) 10px;
}</code></pre>
                </div>
              </section>
              <div class="back-to-top clearfix">
                <a href="#top"><em>Back to Top</em><span class="fa fa-arrow-circle-up"></span></a>
              </div>
            </article>
          </div>
        </div>
      </div>

    </main>

    @@include('footer.inc')

  </body>

</html>
