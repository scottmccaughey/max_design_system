$(function() {

  /**
   * Nav Search Dropdown
   *
   * Open search options dropdown when toggle is clicked.
   */

  $("#dropdown-search-options a").click(function(){
    var selText = $(this).attr('data-name');
    if ( selText ) {
      $('#dropdown-search-toggle').html(selText+' <span class="caret"></span>');
    }
  });


  /**
   * Non-Standard Bootstrap Dropdowns
   *
   * Do not collapse dropdown menus when clicked.
   */

  $('.max-toolbar-container .dropdown-menu').on('click', function(event){
    var events = $._data(document, 'events') || {};
    events = events.click || [];
    for(var i = 0; i < events.length; i++) {
      if(events[i].selector) {
        if($(event.target).is(events[i].selector)) {
          events[i].handler.call(event.target, event);
        }
        $(event.target).parents(events[i].selector).each(function(){
          events[i].handler.call(this, event);
        });
      }
    }
    event.stopPropagation();
  });

});
