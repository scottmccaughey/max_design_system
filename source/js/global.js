/**
 * Sticky Footer
 *
 * Keep footer stuck to the bottom of the screen.
 */

var bumpIt = function() {
  $('body').css('margin-bottom', $('.site-footer').height() + 50);
},
didResize = false;

bumpIt();

$(window).resize(function() {
  didResize = true;
});
setInterval(function() {
  if(didResize) {
    didResize = false;
    bumpIt();
  }
}, 250);


/**
 * Highlight Active Page
 *
 * Add classes to the navigation parent and item based on filename.
 */

$(function() {
  var url      = window.location.pathname;
  var filename = url.substring(url.lastIndexOf('/')+1);
  var slug     = (filename.split('\\').pop().split('/').pop().split('.'))[0];
  var section  = slug.split('-')[0];
  if ( slug === '' || slug == 'index' ) {
    section = 'overview';
    slug    = 'overview-about';
  }
  $('#nav-' + section).addClass('in');
  $('#nav-' + slug).addClass('active');
});


/**
 * Enable/Disable Selector
 *
 * Enable or disable the Bureau select based on the Department/Agency select.
 */

$(document).ready( function() {
  $('#user-dept').bind('change', function (e) {
    if( $('#user-dept').val() == 'doa') {
      $('#user-bureau').prop('disabled', false);
    }
    else {
      $('#user-bureau option:eq(0)').prop('selected', true);
      $('#user-bureau').prop('disabled', true);
    }
  }).trigger('change');
});
