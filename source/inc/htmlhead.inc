<link rel="apple-touch-icon" sizes="57x57" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="//assets.max.gov/max_design_common/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="//assets.max.gov/max_design_common/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="//assets.max.gov/max_design_common/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="//assets.max.gov/max_design_common/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="//assets.max.gov/max_design_common/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="//assets.max.gov/max_design_common/favicons/manifest.json">
<link rel="shortcut icon" href="//assets.max.gov/max_design_common/favicons/favicon.ico">
<meta name="apple-mobile-web-app-title" content="MAX.gov">
<meta name="application-name" content="MAX.gov">
<meta name="msapplication-TileColor" content="#428bca">
<meta name="msapplication-TileImage" content="//assets.max.gov/max_design_common/favicons/mstile-144x144.png">
<meta name="msapplication-config" content="//assets.max.gov/max_design_common/favicons/browserconfig.xml">
<meta name="theme-color" content="#428bca">
<link href="css/all.min.css" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,700,700italic" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
