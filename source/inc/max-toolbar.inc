<a href="#main" class="sr-only sr-only-focusable">Skip to main content</a>
<nav class="max-toolbar-container" role="navigation" aria-label="Main Navigation">
  <div class="navbar navbar-default navbar-static-top">
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown prefs">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="img/toolbar-avatar.jpg" alt="Preferences"> <span class="fa fa-caret-down"></span></a>
        <ul class="dropdown-menu">
          <li class="prefs-header clearfix">
            <img src="img/toolbar-avatar.jpg" alt="Scott McCaughey (OMB, Ctr)">
            <h1>Scott McCaughey<br>(OMB, Ctr)</h1>
          </li>
          <li><a href="#"><span class="fa fa-fw fa-user"></span> My Profile</a></li>
          <li><a href="#"><span class="fa fa-fw fa-user-plus"></span> Set MAX Personal Username</a></li>
          <li><a href="#"><span class="fa fa-fw fa-key"></span> Set/Reset Password</a></li>
          <li><a href="#"><span class="fa fa-fw fa-cog"></span> Manage Personal Settings</a></li>
          <li class="divider"></li>
          <li><a href="#"><span class="fa fa-fw fa-calendar"></span> My Calendar</a></li>
          <li><a href="#"><span class="fa fa-fw fa-desktop"></span> My Shared Desktop</a></li>
          <li class="divider"></li>
          <li><a href="#"><span class="fa fa-fw fa-star"></span> My Favorites</a></li>
          <li><a href="#"><span class="fa fa-fw fa-eye"></span> Recently Viewed</a></li>
          <li><a href="#"><span class="fa fa-fw fa-pencil"></span> Recently Updated</a></li>
          <li class="divider"></li>
          <li><a href="#"><span class="fa fa-fw fa-file-o"></span> Manage Page Watches</a></li>
          <li class="clearfix">
            <a href="#" class="nav-link-primary"><span class="fa fa-fw fa-files-o"></span> MAX Digest</a>
            <a href="#" class="nav-link-secondary"><span class="fa fa-fw fa-cog" title="Manage Digest Settings"></span><span class="sr-only">Manage Digest Settings</span></a>
          </li>
          <li class="divider"></li>
          <li><a href="#"><span class="fa fa-fw fa-sign-out"></span> Log Out</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav">
      <li class="home">
        <a href="#" class="navbar-home dropdown-toggle" data-toggle="dropdown"><em>MAX.gov</em> <span class="fa fa-caret-down"></span></a>
        <ul class="dropdown-menu">
          <li role="presentation" class="dropdown-header">MAX Federal Community</li>
          <li class="clearfix">
            <a href="#" class="nav-link-primary">My Home Space</a>
            <a href="#" class="nav-link-secondary"><span class="fa fa-fw fa-cog" title="Change Home Space"></span><span class="sr-only">Change Home Space</span></a>
          </li>
          <li><a href="#">My Agency</a></li>
          <li><a href="#">Dashboard</a></li>
          <li class="divider"></li>
          <li role="presentation" class="dropdown-header">Other MAX Tools</li>
          <li><a href="#">MAX Calendar</a></li>
          <li><a href="#">MAX Shared Desktop</a></li>
          <li><a href="#">MAX.gov Homepage</a></li>
        </ul>
      </li>
      <li class="find">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Find <span class="fa fa-caret-down"></span></a>
        <ul class="dropdown-menu">
          <li role="presentation" class="dropdown-header">Find People</li>
          <li><a href="#">Find Users &amp; Groups</a></li>
          <li><a href="#">Find MAX Administrators</a></li>
          <li class="divider"></li>
          <li role="presentation" class="dropdown-header">Find Stuff</li>
          <li><a href="#">Agency Communities</a></li>
          <li><a href="#">Cross-Community Topics</a></li>
          <li><a href="#">Government-Wide Communities</a></li>
          <li><a href="#">All Community Spaces</a></li>
          <li class="divider"></li>
          <li role="presentation" class="dropdown-header">My Stuff</li>
          <li><a href="#">My Favorites (Pages &amp; Spaces)</a></li>
          <li><a href="#">My Recently Viewed</a></li>
          <li><a href="#">My Recently Updated</a></li>
          <li><a href="#">My Page Watches</a></li>
          <li><a href="#">My Collaborations</a></li>
          <li class="divider"></li>
          <li><a href="#">Site Search</a></li>
        </ul>
      </li>
      <li class="help">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-star fa-highlight"></span> Help <span class="fa fa-caret-down"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Help</a></li>
          <li><a href="#">Get Started</a></li>
          <li><a href="#">Get Help/Ask a Question</a></li>
          <li><a href="#">Sign Up for Training</a></li>
          <li class="divider"></li>
          <li><a href="#" class="nav-link-highlight"><span class="fa fa-star"></span> New on MAX.gov</a></li>
          <li><a href="#">MAX Cloud Capabilities Presentations</a></li>
          <li><a href="#">MAX Examples &amp; Samples</a></li>
          <li class="divider"></li>
          <li><a href="#">Check My MAX Performance</a></li>
          <li class="divider"></li>
          <li><a href="#">Provide Feedback</a></li>
        </ul>
      </li>
      <li class="contact">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact Us <span class="fa fa-caret-down"></span></a>
        <ul class="dropdown-menu">
          <li>
            <dl class="clearfix">
              <dt>Phone</dt>
              <dd>202-395-6860</dd>
              <dt>Email</dt>
              <dd><a href="mailto:maxsupport@omb.eop.gov">maxsupport@omb.eop.gov</a></dd>
              <dt>Hours</dt>
              <dd>Weekdays — 8:30 AM to 9:00 PM ET<br>Weekends — 9:00 AM to 6:00 PM ET</dd>
            </dl>
            <p class="text-center btn-center">
              <button class="btn btn-primary btn-sm">Provide Feedback</button>
            </p>
          </li>
        </ul>
      </li>
    </ul>
    <div class="navbar-center-wrapper">
      <ul class="nav navbar-nav">
        <li class="perms">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="pull-right">
              <span class="fa fa-lock fa-highlight"></span>
              <span class="fa fa-plus fa-highlight"></span>
              <span class="fa fa-caret-down"></span>
            </span>
            <span class="perm perm-head">Permissions</span>
            <span class="perm perm-body">Specific to Collaboration</span>
          </a>
          <div class="dropdown-menu">
            <div class="panel panel-default">
              <div class="panel-heading">
                <button class="btn btn-xs btn-primary pull-right"><span class="fa fa-lock fa-highlight"></span> Manage Page Permissions</button>
                <h1>Current Page</h1>
              </div>
              <div class="panel-body">
                <p><span class="fa fa-key fa-fw fa-highlight"></span> Security Level: Standard <small>(password, Federated Partner, or higher)</small> <a href="#" class="btn btn-xs btn-default" title="Learn More"><span class="fa fa-question-circle"></span><span class="sr-only">Learn More</span></a></p>
                <p>No View or Edit Restrictions on This Page <small>(Page has inherited View restrictions from parent pages.)</small></p>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h1 class="clearfix">
                  Security Level &amp; View Restrictions Inherited from Parent Pages
                  <div class="checkbox pull-right">
                    <label>
                      <input type="checkbox" checked> Show Only Pages with Restrictions
                    </label>
                  </div>
                </h1>
              </div>
              <div class="panel-body">
                <p>Users will only be able to view the Current Page if they can view the "Parent" page (i.e. they are also included in the view permissions for any "Parent" page with restrictions).</p>
                <p><a href="#">Space: MAX Design Space</a></p>
                <a href="#" class="perm-list"><span class="fa fa-fw fa-file-text-o"></span>Staff Internal Working Pages (Design Space, Restricted)</a> <span class="fa fa-minus-square-o"></span>
                <ul class="perm-list">
                  <li><a href="#"><span class="fa fa-fw fa-users"></span>BFELOB PMO</a> <small>BUDGET-BFELOB-PMO</small></li>
                  <li><a href="#"><span class="fa fa-fw fa-users"></span>MAX Staff and Contractors</a> <small>MAX-STAFF-ALL</small></li>
                  <li><a href="#"><span class="fa fa-fw fa-user"></span>Test User (OMB, Ctr)</a></li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        <li class="search" id="dropdown-search">
          <form class="navbar-form" role="form">
            <div class="input-group">
              <input class="form-control" type="text" name="search" placeholder="Search the MAX Community" title="Enter your search terms">
              <div class="input-group-btn">
                <button id="dropdown-search-toggle" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-target="#dropdown-search" title="View search options">All <span class="fa fa-caret-down"></span></button>
                <button class="btn btn-default btn-go" type="submit" title="Search"><span class="fa fa-fw fa-lg fa-search"></span></button>
              </div>
            </div>
          </form>
          <ul id="dropdown-search-options" class="dropdown-menu">
            <li><a href="#" data-name="All"><span class="fa fa-fw fa-search"></span> All Community</a></li>
            <li><a href="#" data-name="Space"><span class="fa fa-fw fa-search"></span> This Space</a></li>
            <li><a href="#" data-name="Page"><span class="fa fa-fw fa-search"></span> This Page &amp; Children</a></li>
            <li><a href="#"><span class="fa fa-fw"></span> Go to Advanced Search</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
