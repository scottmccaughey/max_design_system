<a href="#main" class="sr-only sr-only-focusable">Skip to main content</a>
<nav class="panel-group panel-group-nav" id="nav-dm" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="nav-overview-heading">
      <div class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#nav-dm" href="#nav-overview" aria-expanded="false" aria-controls="nav-overview">Overview</a>
      </div>
    </div>
    <div id="nav-overview" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nav-overview-heading">
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <li role="presentation" id="nav-overview-about"><a href="index.html">About this Design System</a></li>
          <li role="presentation" id="nav-overview-principles"><a href="overview-principles.html">Design Principles</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="nav-visual-heading">
      <div class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#nav-dm" href="#nav-visual" aria-expanded="false" aria-controls="nav-visual">Visual Identity</a>
      </div>
    </div>
    <div id="nav-visual" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nav-visual-heading">
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <li role="presentation" id="nav-visual-branding"><a href="visual-branding.html">Branding</a></li>
          <li role="presentation" id="nav-visual-colors"><a href="visual-colors.html">Colors</a></li>
          <li role="presentation" id="nav-visual-typography"><a href="visual-typography.html">Typography</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="nav-accessibility-heading">
      <div class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#nav-dm" href="#nav-accessibility" aria-expanded="false" aria-controls="nav-accessibility">Accessibility</a>
      </div>
    </div>
    <div id="nav-accessibility" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nav-accessibility-heading">
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <li role="presentation" id="nav-accessibility-interfaces"><a href="accessibility-interfaces.html">Accessible Interfaces</a></li>
          <li role="presentation" id="nav-accessibility-text"><a href="accessibility-text.html">Text Contrast</a></li>
          <li role="presentation" id="nav-accessibility-skip"><a href="accessibility-skip.html">Skip Navigation</a></li>
          <li role="presentation" id="nav-accessibility-nesting"><a href="accessibility-nesting.html">Nested Headings</a></li>
          <li role="presentation" id="nav-accessibility-auditor"><a href="accessibility-auditor.html">Accessibility Auditor Tool</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="nav-ui-heading">
      <div class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#nav-dm" href="#nav-ui" aria-expanded="false" aria-controls="nav-ui">UI Toolkit</a>
      </div>
    </div>
    <div id="nav-ui" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nav-ui-heading">
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <li role="presentation" id="nav-ui-bootstrap"><a href="ui-bootstrap.html">Bootstrap &amp; the Grid</a></li>
          <li role="presentation" id="nav-ui-browser"><a href="ui-browser.html">Browser &amp; Device Support</a></li>
          <li role="presentation" id="nav-ui-typography"><a href="ui-typography.html">Typographic Hierarchy</a></li>
          <li role="presentation" id="nav-ui-links"><a href="ui-links.html">Links</a></li>
          <li role="presentation" id="nav-ui-buttons"><a href="ui-buttons.html">Buttons</a></li>
          <li role="presentation" id="nav-ui-icons"><a href="ui-icons.html">Icons</a></li>
          <li role="presentation" id="nav-ui-forms"><a href="ui-forms.html">Forms</a></li>
          <li role="presentation" id="nav-ui-forms-templates"><a href="ui-forms-templates.html">Form Templates</a></li>
          <li role="presentation" id="nav-ui-forms-validation"><a href="ui-forms-validation.html">Form Validation</a></li>
          <li role="presentation" id="nav-ui-tables"><a href="ui-tables.html">Tables</a></li>
          <li role="presentation" id="nav-ui-nav"><a href="ui-nav.html">Navigation</a></li>
          <li role="presentation" id="nav-ui-lists"><a href="ui-lists.html">List Groups</a></li>
          <li role="presentation" id="nav-ui-modals"><a href="ui-modals.html">Modals</a></li>
          <li role="presentation" id="nav-ui-misc"><a href="ui-misc.html">Miscellaneous Components</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="nav-code-heading">
      <div class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#nav-dm" href="#nav-code" aria-expanded="false" aria-controls="nav-code">Code Guide</a>
      </div>
    </div>
    <div id="nav-code" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nav-code-heading">
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <li role="presentation" id="nav-code-html"><a href="code-html.html">HTML</a></li>
          <li role="presentation" id="nav-code-css"><a href="code-css.html">CSS</a></li>
          <li role="presentation" id="nav-code-js"><a href="code-js.html">JavaScript</a></li>
          <li role="presentation" id="nav-code-editor"><a href="code-editor.html">Editor Preferences</a></li>
        </ul>
      </div>
    </div>
  </div>
</nav>
