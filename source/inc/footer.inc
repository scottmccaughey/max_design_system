<footer id="site-footer" class="site-footer affix-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h1>About this Project</h1>
        <p>This project is maintained by <a href="http://max.gov">MAX.gov</a>.</p>
        <p>The MAX.gov Design System has been adapted from the several sources. View the <a href="credits.html">Credits</a> for complete details.</p>
      </div>
      <div class="col-sm-6">
        <h1>A Note About Our Standards</h1>
        <p>These standards reflect our latest thinking and are just beginning to be incorporated into MAX.gov’s most recent projects.</p>
        <!--
        <p><a href="#" class="btn btn-default"><span class="fa fa-github-alt fa-left"></span>View on GitHub</a></p>
        -->
      </div>
    </div>
  </div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="js/prism.js"></script>
<script src="js/all.min.js"></script>
