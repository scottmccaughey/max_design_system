// For easy reference, declare all filepaths in one place.
var paths = {
  node: {
    dir:       'node_modules/',
  },
  styles: {
    src:       'source/scss/main.scss',
    dest:      'public/css/',
    watch:     'source/**/*.scss', // Better LiveReload support.
  },
  scripts: {
    src:      ['source/**/*.js'],
    dest:      'public/js/',
  },
  inc: {
    src:       'source/',
    dest:      'public/',
    watchhtml: 'source/*.html',
    watchinc:  'source/inc/*.inc',
  },
  img: {
    src:       'source/img/',
    dest:      'public/img/',
    watch:     'source/img/**/*', // Better LiveReload support.
  }
};

// To make things easy, auto-load all gulp modules.
require('matchdep').filterDev('gulp*').forEach(function( module ) {

  // Remove the `gulp-` prefix & internal hyphens for better module var names.
  var module_name = module.replace(/^gulp-/, '').replace(/-/, '');

  // Then require the module.
  global[module_name] = require(module);

});
var pngquant = require('imagemin-pngquant');
var merge    = require('merge-stream');

gulp.task('styles', function( ) {

	return rubysass(paths.styles.src, {
    loadPath: [
      paths.styles.src,
      paths.node.dir + 'max_ui_base_styles/source/scss',
      paths.node.dir + 'bootstrap-sass/assets/stylesheets',
    ],
    sourcemap: true,
    style: 'compressed'
  })

    .pipe(autoprefixer())

    .pipe(concat('all.min.css'))

    .pipe(sourcemaps.write('.'))

    .pipe(gulp.dest(paths.styles.dest))

    .pipe(livereload());

});

gulp.task('scripts', function( ) {

	var stylish = require('jshint-stylish');

	gulp.src(paths.scripts.src)

    .pipe(jshint({ "loopfunc": true }))

		.pipe(jshint.reporter(stylish))

		.pipe(sourcemaps.init())

		.pipe(concat('all.min.js'))

		.pipe(uglify({ mangle: false }))

		.pipe(sourcemaps.write('.'))

		.pipe(gulp.dest(paths.scripts.dest))

		.pipe(livereload());

});

gulp.task('img', function() { 

  var imgall = gulp.src(paths.img.src + '*');

  var imgsvg = gulp.src(paths.img.src + '*.svg')

    .pipe(svg2png());

  return merge([imgall, imgsvg])

    .pipe(pngquant({quality: '65-80', speed: 4})())

    .pipe(gulp.dest(paths.img.dest))

    .pipe(livereload());

});

gulp.task('inc', function() { 

  gulp.src(paths.inc.src + '*.html')

    .pipe(fileinclude({
      prefix: '@@',
      basepath: paths.inc.src + 'inc/'
    }))

    .pipe(gulp.dest(paths.inc.dest))

    .pipe(livereload());

});

gulp.task('all', ['styles', 'scripts', 'img', 'inc']);

  gulp.task('watch', function() {

  livereload.listen();

  watch(paths.styles.watch, function() {
    gulp.start('styles');
  });

  watch(paths.scripts.src, function() {
    gulp.start('scripts');
  });

  watch(paths.img.watch, function() {
    gulp.start('img');
  });

  watch(paths.inc.watchhtml, function() {
    gulp.start('inc');
  });

  watch(paths.inc.watchinc, function() {
    gulp.start('inc');
  });

});

  gulp.task('default', ['all', 'watch']);
